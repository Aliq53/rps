import datetime
import random
from statistics import mode

from system.constants import Gesture, BEST_OF_ROUNDS
from system.models import Game, Winner, Round


class PlayerVsBot:
    data, game, round = None, None, None
    MAX_ROUNDS = BEST_OF_ROUNDS

    def __init__(self, *args, **kwargs):
        self.data = kwargs.get('data', None)
        self.game = kwargs.get('game', None)
        self.round = kwargs.get('round', None)

    def new_game(self):
        if self.data:
            self.game = Game.objects.create(
                player=self.data.get('player')
            )
        return self.game

    def new_round(self):
        if self.game and self.game.rounds.count() < self.MAX_ROUNDS and self.data:
            self.round = Round.objects.create(
                game=self.game,
                player_gesture=self.data.get('player_gesture'),
                bot_gesture=random.randint(Gesture.ROCK, Gesture.SCISSOR)
            )
            self.evaluate_round()
        return self.round

    def evaluate_round(self):
        if self.round:
            winning_choice, winner = None, Winner.PLAYER
            player_gesture = self.round.player_gesture
            bot_gesture = self.round.bot_gesture
            paper_win_choice = [Gesture.ROCK, Gesture.PAPER]
            rock_win_choice = [Gesture.ROCK, Gesture.SCISSOR]
            if not player_gesture == bot_gesture:
                if player_gesture in paper_win_choice and bot_gesture in paper_win_choice:
                    winning_choice = Gesture.PAPER
                elif player_gesture in rock_win_choice and bot_gesture in rock_win_choice:
                    winning_choice = Gesture.ROCK
                else:
                    winning_choice = Gesture.SCISSOR

            if not winning_choice:
                winner = Winner.TIE
            elif bot_gesture == winning_choice:
                winner = Winner.BOT

            self.round.winner = winner
            self.round.save()
            self.evaluate_game()
        return self.round

    def evaluate_game(self):
        if self.game:
            self.game.refresh_from_db()
            winners = list(self.game.rounds.all().values_list('winner', flat=True))
            if len(set(winners)) < self.MAX_ROUNDS:
                winner = mode(winners)
            else:
                winner = Winner.TIE
            self.game.winner = winner
            self.game.save()
            if self.game.rounds.count() >= self.MAX_ROUNDS:
                self.game.completed_on = datetime.datetime.now()
            self.game.save()
        return self.game
