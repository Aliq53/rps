let newGameCard = $('#new-game');
let newRoundCard = $('#new-round');
let historyCard = $('#history-card');
let historyButton = $('#score-history');
let historyTable = $('#history-tbl')
let currentView = localStorage.getItem('currentView') || null;
let gameId = localStorage.getItem('gameId') || null;
let playerName = localStorage.getItem('playerName') || null;
let roundsCount = localStorage.getItem('roundsCount') || null;
let roundResult = $('#round-result');
let gestureDisabled = false;

function renderScreen(){
    if(gameId && currentView == 'new-round'){
        renderNewRound();
    }else if(currentView == 'history'){
        renderHistory();
    }else{
        renderNewGame();
    }
}

function renderNewGame(){
    historyCard.hide();
    newRoundCard.hide();
    newGameCard.show();
}

function renderNewRound(){
    historyCard.hide();
    newGameCard.hide();
    newRoundCard.show();
    $.ajax({url: `${gameListCreateUrl}${gameId}/`, cache:false}).done(function(response){
        processSuccessResponse(response, nextRound=false);
    });
}

function renderHistory(){
    newRoundCard.hide();
    newGameCard.hide();
    historyCard.show();
    $.ajax({url: `${gameListCreateUrl}history/`, cache:false}).done(function(response){
        if(response.length > 0){
            rows = [];
            response.forEach((game, index) => rows.push($(`<tr><td>${index+1}</td><td>${game.player}</td><td>${game.get_winner_display}</td><td>${game.completed_on}</td></tr>`)));
            let tbody = $(historyTable.find('tbody'));
            tbody.empty();
            tbody.append(rows);
        }
    });
}

$(function() {
    if(playerName){
        $('#player-name').text(playerName);
        $('input[name=player]').val(playerName);
    }
    if(roundsCount){
        $('#rounds-count').text(roundsCount);
    }
    renderScreen();
});

$(historyButton).on('click', function(){
    setCurrentView('history');
});


function setCurrentView(view){
    currentView = view;
    localStorage.setItem('currentView', currentView);
    renderScreen();
}

function setGameId(gId){
    if(gId != null){
        gameId = gId;
        localStorage.setItem('gameId', gameId);
    }else{localStorage.removeItem('gameId');}
}

function setPlayerName(playerName){
    if(playerName != null){
        playerName = playerName;
        localStorage.setItem('playerName', playerName);
        $('#player-name').text(playerName);
    }else{localStorage.removeItem('playerName');}
}

function setRoundsCount(rounds){
    if(rounds != null){
        roundsCount = rounds;
        localStorage.setItem('roundsCount', rounds);
        $('#rounds-count').text(rounds);
    }else{localStorage.removeItem('roundsCount');}
}

function resetGame(){
    setGameId(null);
    setRoundsCount(null);
    setPlayerName(null);
    roundResult.text('');
    gestureDisabled = false;
    $('#another-game').remove();
    setCurrentView('new-game');
}

function processSuccessResponse(response, nextRound=true){
    setGameId(response.id);
    if(response.rounds.length > 0 && response.rounds.length >= roundsCount){
        roundResult.text(`Previous Round Winner was ${response.rounds[roundsCount-1].get_winner_display}`);
    }else if(response.rounds.length == 0){
        $('another-game').remove();
        roundResult.text('');
    }
    if(nextRound || response.winner){
        if (!response.winner){

            setRoundsCount(response.rounds.length + 1);
            setCurrentView('new-round');
        }else{
            $(`<button id="another-game" class="btn btn-sm btn-success" onclick="newGame('${playerName}')">New Game</button>`).appendTo($('#action-btn-container'));
            if(response.winner != 3){
                roundResult.text(`${response.get_winner_display} Won the Game!`);
            }else{roundResult.text("It was a Fair game resulting in a Tie");}
            gestureDisabled = true;
        }
    }
}

$('#new-game-form').on('submit', function(e){
    e.preventDefault();
    let data = new FormData($(this)[0]);
    newGame(data.get('player'));
    return false;
});

function newGame(name){
    setPlayerName(name);
    data = new FormData();
    data.set('player', name)
    $.ajax({
        url: gameListCreateUrl,
        data: data,
        method: 'post',
        dataType: 'json',
        processData: false,
        cache: false,
        contentType: false,
    }).done(function(response){
        processSuccessResponse(response);
    });

}

function playRound(gesture){
    if(!gestureDisabled){
        $.ajax({
            url: roundListCreateUrl,
            data: {
                game: localStorage.getItem('gameId'),
                player_gesture: gesture,
            },
            method: 'post',
            cache: false,
        }).done(function(response){
            processSuccessResponse(response);
        }).fail(function(error){
            errorsText = '';
            for(const [key, value] of Object.entries(error.responseJSON)){errorsText+=`${key}: ${value}<br>`;}
            roundResult.html(errorsText);
        });
    }else{roundResult.text("Game is Over! Kindly reset or start a new game :)");}
}