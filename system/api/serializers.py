from rest_framework import serializers

from system.constants import BEST_OF_ROUNDS, Winner
from system.models import Game, Round


class RoundSerializer(serializers.ModelSerializer):

    class Meta:
        model = Round
        fields = ['id', 'game', 'player_gesture', 'bot_gesture', 'winner', 'get_winner_display']

    def validate_game(self, game):
        if game.completed_on:
            raise serializers.ValidationError("In-Valid Game, Game is already complete")
        return game


class GameSerializer(serializers.ModelSerializer):
    rounds = RoundSerializer(read_only=True, many=True)
    winner = serializers.SerializerMethodField()
    completed_on = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = ['id', 'player', 'winner', 'completed_on', 'rounds', 'get_winner_display']

    def get_winner(self, obj):
        winner = None
        if obj.completed_on:
            winner = obj.winner
        return winner

    def get_completed_on(self, obj):
        value = None
        if obj.completed_on:
            value = obj.completed_on.strftime('%d-%m-%Y %H:%M:%S')
        return value
