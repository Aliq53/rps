# Simple Rock, Paper & Scissors  Project

##_Description:_
This is a simple game of RPS made in Django using django-rest-framework

##Features:
- Records Player Name
- Maintains Round Summary
- Maintains Game Summary based on Round Summary
- History Score Table

##Project Deployment:
- Install all packages mentioned in requirements.txt file ```pip install -r requirements.txt ```
- Dummy sqlite database is already included in the project
- Run project using ```manage.py runserver ```