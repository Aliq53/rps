from django.db import models

BEST_OF_ROUNDS = 3


class Gesture(models.IntegerChoices):
    ROCK = 1, "Rock"
    PAPER = 2, "Paper"
    SCISSOR = 3, "Scissor"


class Winner(models.IntegerChoices):
    PLAYER = 1, "Player"
    BOT = 2, "Bot"
    TIE = 3, "Tie"
