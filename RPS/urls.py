from django.contrib import admin
from django.urls import path, include

from system.api.urls import system_api_router
from system.views import Home

api_urls = [
    path('system/', include(system_api_router.urls))
]

urlpatterns = [
    path('', Home.as_view(), name="index"),
    path('admin/', admin.site.urls),
    path('api/', include(api_urls))
]
