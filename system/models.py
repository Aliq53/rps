import datetime

from django.db import models

from system.constants import Winner, Gesture
from RPS.models import BaseModel


class Game(BaseModel):
    player = models.CharField(max_length=30)
    completed_on = models.DateTimeField(null=True, blank=True, editable=False)
    winner = models.PositiveSmallIntegerField(choices=Winner.choices, default=Winner.TIE, editable=False)

    def __str__(self):
        return f"{self.player}"


class Round(BaseModel):
    game = models.ForeignKey(Game, on_delete=models.CASCADE, related_name="rounds")
    player_gesture = models.PositiveIntegerField(choices=Gesture.choices, default=Gesture.ROCK)
    bot_gesture = models.PositiveIntegerField(choices=Gesture.choices, default=Gesture.ROCK, editable=False)
    winner = models.PositiveSmallIntegerField(choices=Winner.choices, default=Winner.TIE, editable=False)

    def __str__(self):
        return f"{self.game} {self.get_player_gesture_display()} {self.get_bot_gesture_display()}"
