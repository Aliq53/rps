from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from system.api.serializers import GameSerializer, RoundSerializer
from system.models import Game, Round
from system.workflows.player_vs_bot import PlayerVsBot


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.filter()
    serializer_class = GameSerializer
    http_method_names = ['get', 'post', 'options', 'head', 'trace']

    @action(detail=False, url_path='history', methods=['get'])
    def history(self, request, *args, **kwargs):
        qs = Game.objects.filter(completed_on__isnull=False).order_by('-completed_on')
        serializer = self.get_serializer(qs, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        game_service = PlayerVsBot(data=serializer.validated_data)
        game = game_service.new_game()
        data = self.serializer_class(instance=game).data
        return Response(data, status=status.HTTP_201_CREATED)


class RoundViewSet(viewsets.ModelViewSet):
    queryset = Round.objects.filter()
    serializer_class = RoundSerializer
    http_method_names = ['get', 'post', 'options', 'head', 'trace']

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        game_service = PlayerVsBot(data=data, game=data.get('game'))
        round = game_service.new_round()
        response_data = GameSerializer(instance=round.game).data
        return Response(response_data, status=status.HTTP_201_CREATED)
