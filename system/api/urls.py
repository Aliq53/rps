from rest_framework.routers import DefaultRouter

from system.api.views import GameViewSet, RoundViewSet

system_api_router = DefaultRouter()
system_api_router.register('games', GameViewSet)
system_api_router.register('rounds', RoundViewSet)
