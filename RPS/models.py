import uuid

from django.contrib.auth.models import User
from django.db import models
from django_currentuser.middleware import get_current_authenticated_user


class BaseModel(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, unique=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)
    created_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="created_%(app_label)s_%(class)s", editable=False
    )
    updated_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="updated_%(app_label)s_%(class)s", editable=False
    )

    objects = models.Manager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.created_by = get_current_authenticated_user()
        self.updated_by = get_current_authenticated_user()
        super().save(*args, **kwargs)
