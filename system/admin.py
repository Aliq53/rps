from django.contrib import admin
from django.contrib.auth.models import User, Group

from system.models import Game, Round


class RoundInLineAdmin(admin.TabularInline):
    model = Round
    max_num = 3
    # min_num = 3
    readonly_fields = ['player_gesture', 'bot_gesture', 'winner']

    def get_queryset(self, request):
        qs = super(RoundInLineAdmin, self).get_queryset(request)
        return qs.order_by('created_at')


@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ['player', 'winner', 'completed_on', 'created_at']
    inlines = [RoundInLineAdmin]
    readonly_fields = ['completed_on', 'winner']


@admin.register(Round)
class RoundAdmin(admin.ModelAdmin):
    list_display = ['game', 'player_gesture', 'bot_gesture', 'winner']
    readonly_fields = ['game', 'bot_gesture', 'player_gesture', 'winner']


admin.site.unregister(User)
admin.site.unregister(Group)
